export class User {
    constructor(public firstName: string, public lastName: string, public gender: string, public _id: string, public departure: string,
                public email: string, public topics: string[]) {
    }
}

